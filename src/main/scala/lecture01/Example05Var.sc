var c = 1
c

c = 2
c

//c = 3.14
//Error:(7, 5) type mismatch;
//found   : Double(3.14)
//required: Int

def increaseC() = c = c + 10

increaseC()

println(c)
