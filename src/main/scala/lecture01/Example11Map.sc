val capitals = Map(
  "Russia" -> "Moscow",
  "Great Britain" -> "London"
) + ("Nowhere" -> "Nocity")

capitals("Great Britain")

capitals.contains("Moon")

capitals
  .map(kv => kv._1 * 2 -> kv._2 * 3)
  .filterNot(_._1 == "NowhereNowhere")
  .foreach(println)
