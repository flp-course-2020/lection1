def questionOfLife(b: Boolean) =
  if (b) 42 else "Answer"

questionOfLife(true) match {
  case 13 => "unlucky"
  case 42 => "that's better"
  case "Answer" => "what?"
  case _ => "whatever"
}


val distance = (2.0, "mi")

def toKM(d: (Double, String)): Double = d match {
  case (km, "km") => km
  case (miles, "mi") => miles * 1.6
}

toKM(distance)
