package lecture01

package foo {
  class Data(value: Int)
}

object Example14Package {
  import foo._

  new Data(42)
}
