
val cake = {
  val dough = "dough"
  def bake(s: String) = "baked " + dough
  val biscuit = bake(dough)

  val icing = "icing"

  biscuit + ", " + icing + ", " + biscuit
}

def complexStuff(x: Int) = {
  def multiply(m: Int) = x * m
  def increase(x: Int) = x + 500

  println("value of this expression is ignored")
  increase(multiply(1000))
}

val repeat = { (x: Int, y: String) =>
  y * x
}
