def twice(b: Boolean, x: => String): String =
  if (b) x * 2 else "no need"

def slowCalculation: Int = {
  println("Sooo slooow")

  42
}


twice(true, slowCalculation.toString)

twice(false, slowCalculation.toString)
