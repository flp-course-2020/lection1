val a: Int = 37

val b = 42

//val e: Int = 2.72
//Error:(5, 14) type mismatch;
//found   : Double(2.72)
//required: Int

//a = 5
//Error:(10, 3) reassignment to val

val c = a + b

val result = println(c)
