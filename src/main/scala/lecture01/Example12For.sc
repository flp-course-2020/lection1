val numbers = List(1, 2, 3)

val printed = for {
  n1 <- numbers
  n2 <- numbers
} println(n1 * n2)

val squares =
  for (n <- numbers) yield n * n


val otherNumbers = List(4, 5, 6)

val complexStuff = for {
  low <- numbers if low % 2 == 0
  high <- otherNumbers
} yield low * high


val products = for {
  k <- 10 to 13
  n <- numbers
} yield k * n

println(products)
