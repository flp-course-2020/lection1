val tuple2: (String, Int) = ("foo", 12)
val tuple3 = ("foo", 12, .42)
val tuple4 = ("foo", 12, .42, "bar")
val tuple5 = ("foo", 12, .42, "bar", "baz")
  //...
val tuple22 = ("foo", 12, "foo", 12, .42, "bar", "baz",
  "foo", 12, .42, "bar", "baz", "foo", 12, .42, "bar",
  "baz", "foo", 12, .42, "bar", "baz")

tuple2._1
tuple2._2

val (foo, bar) = tuple2

foo
