val a = 12
val b = 4.5
val c = "example string"

"String concatination: " + a + ", " + b +
  ", " + c.toUpperCase

s"String interpolation $a, $b, ${c.toUpperCase}"
