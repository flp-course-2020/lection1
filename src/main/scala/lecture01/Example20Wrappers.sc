Map(
  "key1" -> 1,
  "key2" -> 2
)

Map(
  ("key1", 1),
  ("key2", 2)
)

"key1" -> 1
"key1" -> 1 == ("key1", 1)

for (n <- 1 to 10) yield n * 2

for (n <- (Range.inclusive(1, 10))) yield n * 2

1 to 10
(1 to 10) == Range.inclusive(1, 10)
