trait Printable {
  def print(): Unit
}

class Vehicle(description: String) extends Printable {
  println("constructing...")

  val name = description + " vehicle"

  def print(): Unit = println(name)
}


val heavy = new Vehicle("heavy")

heavy.print()


object Vehicle {
  def produce(): Vehicle = new Vehicle("from factory")
}

val fromFactory = Vehicle.produce()

fromFactory.print()
