def square(x: Int): Int = x * x

def increase(x: Int, y: Int) = x + y

increase(37, 42)


val repeat = (x: Int, y: String) => y * x

repeat(10, "foo")
